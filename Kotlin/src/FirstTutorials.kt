import kotlin.math.pow

// Autor: Carlos Mestas

// Printing texts
fun main00(){
    println("Prueba de mensaje\n?")
    println("Prueba 2 \t de mensaje")
    println("Es similar a Java?")
    print("Donde estan los ; :\"v")
}

// Using var, val, integer, boolean
fun main01(){
    // Variable name
    // Any type of variable, don't like Java
    var myVariable: Int = 55
    println(myVariable)
    // Isn't necesary specify the type
    var myVariable2 = 5
    println(myVariable2)
    // It's necesary write the symbol $ to call variables
    var myVariable3 = 6
    println("The value of our variable is: $myVariable3")
    myVariable3 = 7
    println("The new value of our variable is: $myVariable3")
    // val means you cannot change his value
    val myVariable4 = 8
    println("The value of a new type of variable is: $myVariable4")
    // booleans
    var myVariable5: Boolean = true
    println("The value of our boolean variable is: $myVariable5")
    myVariable5 = false
    println("The new value of our boolean variable is: $myVariable5")
}

// Using arithmetic operators
fun main02(){
    val x = 23
    val y = 7
    val result = x + y
    println("$x + $y = $result")
    val result2 = 23 - 7
    println("$result2")
    println(23 * 7)
    // int / int = int
    println(23 / 7)
    var x2 = 15
    var y2 = 6
    println("The result of x2 + y2 is: ${x2 + y2}")
    x2 = 100
    y2 = -5
    println("The result of x2 + y2 is now: ${x2 + y2}")
    x2 += 50
    y2 -= -25
    println("The result v2 of x2 + y2 is now: ${x2 + y2}")
}

// Decimal numbers
fun main03(){
    // Two ways to declare a Float variable
    var x: Float = 20.1523564565452321388F
    var y = 8F
    println("The result of  x / y is: ${x / y}")

    // Declare double
    var x2 = 20.1523564565452321388
    println("The result of x2 / y is: ${x2 / y}")

    // Calculate volumen of a sphere
    var pi = 3.141516F
    var radius = 5.5F
    var volumen = 0.75 * pi * radius.pow(3)
    println("The volume of the sphere with radius 5.5 is $volumen")
}

// Strings
fun main04(){
    val string = "This is an example test"
    println("This is an example test without variable string")
    println(string)
    println("Our string is: $string")
    println("Our string is: ${string.uppercase()}")
    println("Our string is: ${string.uppercase().lowercase()}")
    var myNickName = "meca";
    println("My lastname is: ${myNickName.uppercase().reversed()}" )
}

// Logical and comparison operators
fun main05(){
    val x = 4
    val y = 5
    println(x == y)
    println(x < y)
    println(x <= y)
    val x2 = 5
    val y2 = 5
    println(x2 == y2)
    println(x2 != y2)
    println(x2 > y2)
    println(x2 >= y2)

    val simpleExpresion = 3 > 4 || 4 > 3 && 4 <=4;
    val bool = true
    val a = 9
    val b = 3
    val c = 9
    val hardExpresion = !(a != c) && bool || c > (a + b) && (!bool || b < c)
    println("Simple expresion is: $simpleExpresion")
    println("Hard expression is: $hardExpresion")
}

// If conditions
fun main06(){
    val x = 5
    val y = 7
    if (x < y)
        println("x is lesser than y")
    println("This will always be executed 1")
    val x2 = 8
    val y2 = 7
    if (x2 < y2)
        println("x2 is lesser than y2")
    else if(x2 > y2)
        println("x2 bigger than y2")
    println("This will always be executed 2")
    val x3 = 7
    val y3 = 7
    if(x3 < y3) {
        println("x3 is lesser than y3")
    }
    else if (x3 > y3){
        println("x3 is bigger than y3")
    }
    else {
        println("x3 is equal to y3")
    }
    println("This will always be executed 3")
    val a = 7
    val b = 7
    val c = if(a + b == 14) 3 else 10
    val d = if(a - b == 14) 5 else 10
    println("The value of c is: $c")
    println("The value of d is: $d")

    val string01 = "car"
    val string02 = "racecar"
    println("The string is $string01")
    if(string01.equals(string01.reversed()))
        println("The string $string01 is a palindrome")
    else
        println("The string $string01 is not a palindrome")
    println("The string is $string02")
    if(string02.equals(string02.reversed()))
        println("The string $string02 is a palindrome")
    else
        println("The string $string02 is not a palindrome")
}

// Null values and user input
fun main07(){

}